import 'reflect-metadata';
import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import cors from 'cors';
import { errors } from 'celebrate';
import routes from './routes';
import AppError from '@shared/errors/AppError';
import '@shared/typeorm';

const app = express();

app.use(cors());

//Define que a API vai interpretar Json como padrão
app.use(express.json());

//Faz o app usar o routes
app.use(routes);

app.use(errors());

//Captura qualquer exceção e envia para nossa classe de tratamento de exceções
app.use(
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    if (error instanceof AppError) {
      return response.status(error.statusCode).json({
        status: 'error',
        statusCode: error.statusCode,
        message: error.message,
      });
    }

    console.log(error);

    return response.status(500).json({
      status: 'error',
      statusCode: 500,
      message: 'Ínternal server error',
    });
  },
);

app.listen(3334, () => {
  console.log('Server started on port 3334! 🏆');
});
